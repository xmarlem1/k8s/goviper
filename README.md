# goviper in k8s


Esempio di hot reload della configurazione da una configmap se questa viene aggiornata.
L'applicazione go e' in grado di rilevare il cambiamento e scrivere la cosa nel log. e.g.

```bash
Listening on port 80...
WARN[0006] Config file changed                           file=/Users/marco/Dev/golang/playviper/config.yaml
```

NB. quest'app puo' girare su rancher desktop senza dover pushare l'image su remote registry...

How?

`make build_local`
`make deploy`
