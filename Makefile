PHONY: build push


# IMAGE_NAME=$(shell uuidgen | head -c 8 | tr 'A-Z' 'a-z')
IMAGE_NAME=goviper

build_push:
	nerdctl build -t ttl.sh/${IMAGE_NAME}:1h .
	nerdctl push ttl.sh/${IMAGE_NAME}:1h
	echo "Pushed: ${IMAGE_NAME}"

build_local:
	nerdctl -n k8s.io build -t ${IMAGE_NAME}:latest .
	echo "Pushed: ${IMAGE_NAME}"

deploy:
	kubectl apply -k k8s

undeploy:
	kubectl delete -k k8s
