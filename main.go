package main

import (
  "fmt"
  "log"
  "net/http"
  "os"
  "path/filepath"

  "github.com/fsnotify/fsnotify"
  "github.com/sirupsen/logrus"
  "github.com/spf13/pflag"
  "github.com/spf13/viper"
  _ "github.com/spf13/viper/remote"
)

// posso anche definire dei parametri da linea di comando
func init() {
  fmt.Println("initializing...")

  pflag.IntP("port", "p", 8080, "Port number")
  pflag.BoolP("load-keys-at-startup", "l", false, "load keys at startup")

  pflag.Parse()

  viper.BindPFlag("port", pflag.Lookup("port"))

}

func main() {

  viper.SetDefault("name", "marco")

  viper.SetConfigName("config")
  viper.SetConfigType("yaml")
  viper.AddConfigPath(".")
  viper.AddConfigPath("/etc/playviper/")
  home, _ := os.UserHomeDir()

  viper.AddConfigPath(filepath.Join(home, ".playviper"))

  if err := viper.ReadInConfig(); err != nil {
    if _, ok := err.(viper.ConfigFileNotFoundError); ok {
      //
      log.Println("Config file not found. Will read environment variables...")
    } else {
      // config file found but another error was produced
      panic(fmt.Errorf("fatal error config file: %w", err))
    }
  }

  viper.AutomaticEnv()
  viper.SetEnvPrefix("play")

  viper.WatchConfig()
  viper.OnConfigChange(func(e fsnotify.Event) {
    logrus.WithField("file", e.Name).Warn("Config file changed")
  })

  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Config: %s\n", viper.GetString("server.hostname"))
  })

  fmt.Println("Listening on port 80...")
  log.Fatal(http.ListenAndServe("0.0.0.0:80", nil))

  // connect remote etcd

  // viper.AddRemoteProvider("etcd3", "http://localhost:2379", "/config/playviper.yaml")
  // err := viper.ReadRemoteConfig()
  // if err != nil {
  //  panic(err)
  // }

  // fmt.Println(viper.GetString("server.hostname"))
  //  fmt.Println(viper.GetString("attestors.type"))

  // fmt.Println(viper.GetInt("port"))

}
